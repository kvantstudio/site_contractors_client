<?php

namespace Drupal\site_contractors_client\TwigExtension;

use Drupal\site_contractors_client\Form\ContractorsClientAddForm;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class ContractorsClientTwigExtension extends \Twig_Extension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('contractorsClientAddForm', array($this, 'contractorsClientAddForm')),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_contractors_client.twig_extension';
  }

  /**
   * Профиль - форма регистрация по номеру телефона
   */
  function contractorsClientAddForm() {
    $form = new ContractorsClientAddForm();
    $form = \Drupal::formBuilder()->getForm($form);
    return $form;
  }
}
