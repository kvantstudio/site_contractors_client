<?php

namespace Drupal\site_contractors_client\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the Contractor field default widget.
 *
 * @FieldWidget(
 *   id = "site_contractors_client_contractor_default_widget",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_contractors_client_contractor"
 *   }
 * )
 */
class ContractorDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $contractors = $this->getContractors();
    $element['target_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Contractor'),
      '#options' => $contractors,
      '#default_value' => isset($items[$delta]->target_id) ? $items[$delta]->target_id : 0,
      '#empty_value' => 0,
      '#access' => TRUE,
    ];

    return $element;
  }

  /**
   * Формирует список контрагентов доступных текущему пользователю.
   */
  private function getContractors() {
    $data = \Drupal::service('site_contractors_client.database')->getContractorsForUser(\Drupal::currentUser()->id());
    return $data;
  }
}
