<?php

namespace Drupal\site_contractors_client\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Contractor field type.
 *
 * @FieldType(
 *   id = "site_contractors_client_contractor",
 *   label = @Translation("Contractor"),
 *   description = @Translation("List of contractors."),
 *   module = "site_contractors_client",
 *   category = @Translation("Contractors (client)"),
 *   default_widget = "site_contractors_client_contractor_default_widget",
 *   default_formatter = "site_contractors_client_contractor_default_formatter"
 * )
 */
class Contractor extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Contractor'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'target_id' => [
          'type' => 'int',
          'not null' => FALSE,
          'unsigned' => TRUE,
          'default' => 0,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = TRUE;
    if ($this->get('target_id')->getValue()) {
      $isEmpty = FALSE;
    }

    return $isEmpty;
  }
}
