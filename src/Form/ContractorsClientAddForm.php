<?php

/**
 * @file
 * Contains Drupal\site_contractors_client\Form\ContractorsClientAddForm
 */

namespace Drupal\site_contractors_client\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ContractorsClientAddForm extends FormBase {

  /**
   * Название кнопки создания контрагента.
   *
   * @var int
   */
  protected $label;

  /**
   * Конструктор формы.
   *
   * @param [type] $nid
   */
  public function __construct(string $label = 'Add organization') {
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_contractors_client_add_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    // Токен доступа.
    $form['inn'] = [
      '#type' => 'number',
      '#title' => $this->t('TIN of organization'),
      '#title_display' => FALSE,
      '#attributes' => ['placeholder' => $this->t('TIN of organization')],
      '#default_value' => "",
      '#min' => 0,
      '#max' => 999999999999,
      '#size' => 15,
      '#maxlength' => 12,
      '#required' => TRUE,
    ];

    // Submit button, for submitting the form results.
    $form['actions'] = ['#type' => 'actions', '#weight' => 20];
    $form['actions']['submit_tin'] = [
      '#type' => 'button',
      '#value' => $this->t($this->label),
      '#attributes' => ['class' => []],
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
      ],
    ];

    // $form['#attached']['library'][] = 'site_contractors_client/form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Сохранение данных на сервере.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Выполняет стандартную валидацию полей формы и добавляет примечания об ошибках.
    FormBase::validateForm($form, $form_state);

    if (!drupal_get_messages() && !$form_state->getValue('validate_error')) {
      // Профиль полдьзователя.
      $uid = $form_state->getValue('uid');
      $profile = UserAccount::getUserProfile($uid);

      $triggerdElement = $form_state->getTriggeringElement();
      $type_notify = $triggerdElement['#type_notify'];

      $user_data = \Drupal::service('user.data');
      $current_settings = (int) $user_data->get('site_contractors_client', $profile->uid, $type_notify);
      if ($current_settings && $current_settings === 1) {
        $user_data->set('site_contractors_client', $profile->uid, $type_notify, 0);
        $response->addCommand(new InvokeCommand('.site-linline-account-form__btn_' . $type_notify, 'removeClass', array('site-linline-account-form__btn_active')));
      } else {
        $user_data->set('site_contractors_client', $profile->uid, $type_notify, 1);
        $response->addCommand(new InvokeCommand('.site-linline-account-form__btn_' . $type_notify, 'addClass', array('site-linline-account-form__btn_active')));
      }
    } else {
      $response->addCommand(new AlertCommand('Возникла ошибка...'));
    }

    return $response;
  }
}
