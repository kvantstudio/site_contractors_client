<?php

namespace Drupal\site_contractors_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SettingsForm.
 */
class ContractorsClientSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_contractors_client_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_contractors_client.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('site_contractors_client.settings');

    // Настройки авторизации и регистрации по API.
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API settings'),
    ];

    // Токен доступа.
    $form['settings']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#default_value' => $config->get('access_token') ? $config->get('access_token') : "",
      '#required' => TRUE,
    ];

    // Токен доступа.
    $form['settings']['remote_server'] = [
      '#type' => 'url',
      '#title' => $this->t('Address of the server for making API requests'),
      '#default_value' => $config->get('remote_server') ? $config->get('remote_server') : "",
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = \Drupal::service('config.factory')->getEditable('site_contractors_client.settings');
    $config->set('access_token', trim($form_state->getValue('access_token')));
    $config->set('remote_server', trim($form_state->getValue('remote_server')));

    $config->save();
  }
}
