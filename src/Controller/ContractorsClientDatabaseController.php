<?php

namespace Drupal\site_contractors_client\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContractorsClientDatabaseController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Создает организацию.
   */
  public function createContractor(int $inn) {
    // Время генерации запроса.
    $request_time = \Drupal::time()->getRequestTime();

    // Загружаем конфигурацию.
    $config = \Drupal::config('site_contractors_client.settings');
    $access_token = $config->get('access_token');
    $remote_server = $config->get('remote_server');

    // Получаем контрагента.
    $url = $remote_server . "/api/v1/get-contractor?_format=json&token=$access_token&inn=$inn";
    $response = \Drupal::service('kvantstudio.requests')->curl($url);

    // Регистрируем контрагента.
    if ($response && $response['status']) {
      $result = $response['result'];
      $fields = [
        'uuid' => $result['contractor']['uuid'],
        'inn' => $result['contractor']['iin'],
        'name' => $result['contractor']['name'],
        'description' => '',
        'created' => $request_time,
      ];
      $query = $this->connection->insert('site_contractors');
      $query->fields($fields);

      return $query->execute();
    }

    return FALSE;
  }

  /**
   * Назначает пользователю привязку к организации.
   */
  public function setContractorForUser(int $cid, int $uid, int $access) {
    $fields = [
      'cid' => $cid,
      'uid' => $uid,
      'access' => $access,
    ];
    $query = $this->connection->insert('site_contractors_users');
    $query->fields($fields);

    return $query->execute();
  }

  /**
   * Возвращает объект контрагента по ID.
   */
  public function loadContractorById(int $cid) {
    $query = $this->connection->select('site_contractors', 'n');
    $query->fields('n');
    $query->condition('n.cid', $cid);
    return $query->execute()->fetchObject();
  }

  /**
   * Возвращает объект контрагента по ИНН.
   */
  public function loadContractorByINN(int $inn) {
    $query = $this->connection->select('site_contractors', 'n');
    $query->fields('n');
    $query->condition('n.inn', $inn);
    return $query->execute()->fetchObject();
  }

  /**
   * Возвращает массив контрагентов, доступных для отображения учётной записи пользователя.
   */
  public function getContractorsForUser(int $uid) {
    $account = \Drupal\user\Entity\User::load($uid);

    $query = $this->connection->select('site_contractors', 'n');
    $query->fields('n', ['cid', 'name']);

    if (!$account->hasPermission('site contractors allow view all contractors')) {
      $query->join('site_contractors_users', 'u', 'u.cid = n.cid');
      $query->condition('u.access', 0, '>');
      $query->condition('u.uid', $uid);
    }

    $query->orderBy('n.name', 'ASC');
    $result = $query->execute();

    $contractors = [];
    foreach ($result as $row) {
      $contractors[$row->cid] = Html::escape($row->name);
    }

    return $contractors;
  }
}
